module RecordStamps
  class RecordStamp
    attr_accessor :name,
                  :actions

    def initialize(stamp_def)
      unless stamp_def.is_a? Hash
        stamp_def = { name: stamp_def.to_sym, actions: {} }
      end

      self.name = stamp_def[:name]
      self.actions = stamp_def[:actions]

      set_default_actions
    end

    private

    def set_default_actions
      # Actions are not defined for basic actions.
      return if name.in?(%i[created updated])

      actions[:query] ||= default_query_action_name
      actions[:set] ||= default_set_action_name

      # Unset actions is not provided by default.
      actions[:unset] = default_unset_action_name if actions[:unset] == true
    end

    def default_query_action_name
      "#{name}?"
    end

    def default_set_action_name
      case name
      when :activated then 'activate!'
      when :confirmed then 'confirmed!'
      when :disabled then 'disable!'
      when :deleted then 'delete!'
      when :enabled then 'enable!'
      else "#{name}!"
      end
    end

    def default_unset_action_name
      case name
      when :activated then 'deactivate!'
      when :enabled then 'disable!'
      when :disabled then 'enable!'
      else "un#{name}!"
      end
    end
  end
end
