module ActiveRecord
  module ConnectionAdapters
    module TableStamps
      extend ActiveSupport::Concern

      included do
        def stamp(*stamp)
          @base.add_stamps(name, *stamp)
        end
      end
    end
  end
end

ActiveRecord::ConnectionAdapters::Table
  .include(ActiveRecord::ConnectionAdapters::TableStamps)
