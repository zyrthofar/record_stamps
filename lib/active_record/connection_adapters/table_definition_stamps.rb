module ActiveRecord
  module ConnectionAdapters
    module TableDefinitionStamps
      extend ActiveSupport::Concern

      included do
        def stamps(*stamps)
          options = stamps.extract_options!

          stamps.each do |stamp|
            time_attribute = (stamp.to_s + '_at').to_sym
            user_attribute = (stamp.to_s + '_by').to_sym

            nullable = options[:null] || stamp != :created

            column(time_attribute,
                   :datetime,
                   null: nullable)

            references(user_attribute,
                       foreign_key: { to_table: :users },
                       null: true)
          end
        end
      end
    end
  end
end

ActiveRecord::ConnectionAdapters::TableDefinition
  .include(ActiveRecord::ConnectionAdapters::TableDefinitionStamps)
