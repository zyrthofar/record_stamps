module ActiveRecord
  module ConnectionAdapters
    module SchemaStatementsStamps
      extend ActiveSupport::Concern

      included do
        def add_stamp(table_name, *stamp)
          options = stamp.extract_options!

          time_attribute = (stamp.to_s + '_at').to_sym
          user_attribute = (stamp.to_s + '_by').to_sym

          nullable = options[:null] || stamp != :created

          add_column(table_name,
                     time_attribute,
                     :datetime,
                     null: nullable)

          add_reference(table_name,
                        user_attribute,
                        foreign_key: { to_table: :users },
                        null: true)
        end
      end
    end
  end
end

ActiveRecord::ConnectionAdapters::SchemaStatements
  .include(ActiveRecord::ConnectionAdapters::SchemaStatementsStamps)
