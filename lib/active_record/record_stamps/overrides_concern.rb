module ActiveRecord
  module RecordStamps
    module OverridesConcern
      extend ActiveSupport::Concern

      def initialize_dup(other)
        super

        self
          .class
          .record_stamps
          .keys
          .flat_map { |stamp_name| self.class.stamp_attributes(stamp_name) }
          .reject { |attribute| attribute.in?(%i[created_at updated_at]) }
          .select { |attribute| has_attribute?(attribute) }
          .each do |attribute|
            self[attribute] = nil
            clear_attribute_changes([attribute])
          end
      end

      private

      def _create_record
        if record_stamps
          set_user_stamp(:created, default_user)
          set_user_stamp(:updated, default_user)
        end

        super
      end

      def _update_record(*args, touch: true, **_options)
        if touch && record_stamps && (!partial_writes? || changed?)
          set_user_stamp(:updated, default_user)
        end

        super(*args)
      end
    end
  end
end
