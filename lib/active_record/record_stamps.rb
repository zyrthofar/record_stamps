# Record stamps allow you to remember when an action last took place, and who
# did it. They offer automatic functionalities similar to the `:created` and
# `:updated` provided by `ActiveRecord`. These attributes are in fact augmented
# with a reference to the `User` model.
#
# Stamping a record updates the associated time and user stamps. The user is
# determined by `Thread.current[:current_user]`, so must be set, for example,
# to the authenticated user in a controller's `before_action`.
#
# Aside from the `(stamped)_at` and `(stamped)_by_id` attributes, the record
# has a reference to the `User` through the `(stamped)_by` association. It also
# has methods for querying the stamp, `(stamped)?`, and for stamping the
# record, `(stamped)!`. A method to unset the stamp, `un(stamped)!`, can also
# be provided, if required, by setting if to `true`. These method names can all
# be overridden by using the `query`, `set`, and `unset` action keys.
#
# A model is given stamps by means of the `stamps` method:
#   stamps :created,
#          :updated,
#          { name: :taught, actions: { set: 'teach!' } },
#          { name: :disabled, actions: { unset: true } }
#
# This previous example will add the following:
#   * `:created_at` attribute;
#   * `:created_by_id` attribute;
#   * `:created_by` association;
#   * `:updated_at` attribute;
#   * `:updated_by_id` attribute;
#   * `:updated_by` association;
#   * `:taught_at` attribute;
#   * `:taught_by_id` attribute;
#   * `:taught_by` association;
#   * `:taught?` query action method;
#   * `:teach!` set action method;
#   * `:disabled_at` attribute;
#   * `:disabled_by_id` attribute;
#   * `:disabled_by` association;
#   * `:disabled?` query action method;
#   * `:disable!` set action method; and
#   * `:enable!` unset action method.
#
# Default action names are `(stamped)?`, `(stamped)!`, and `un(stamped)!`.
# Some stamps, such as `:disabled` in the example, have different default
# action names, and this is why the `set` and `unset` action names did not have
# to be manually specified.
#
# Similarly to the default implementation of `:created` and `:updated`, all
# stamps are cleared when duplicating a record using `#dup`. As such, stamps
# must not be used when the state of a record should be transferred.

require 'active_record/record_stamps/overrides_concern'

module ActiveRecord
  module RecordStamps
    extend ActiveSupport::Concern
    include OverridesConcern

    included do
      class_attribute :record_stamps
      self.record_stamps = {}

      class << self
        def stamps(*stamp_defs)
          stamp_defs.each { |stamp_def| stamp(stamp_def) }
        end

        def stamp(stamp_def)
          stamp = ::RecordStamps::RecordStamp.new(stamp_def)

          record_stamps[stamp.name] = stamp

          attribute time_stamp_attribute(stamp.name),
                    :datetime

          belongs_to user_stamp_attribute(stamp.name),
                     class_name: User.name,
                     optional: true

          create_methods(stamp)
        end

        def time_stamp_attribute(stamp_name)
          "#{stamp_name}_at".to_sym
        end

        def user_stamp_attribute(stamp_name)
          "#{stamp_name}_by".to_sym
        end

        def user_id_stamp_attribute(stamp_name)
          user_attribute = user_stamp_attribute(stamp_name)
          "#{user_attribute}_id".to_sym
        end

        def stamp_attributes(stamp_name)
          [
            time_stamp_attribute(stamp_name),
            user_id_stamp_attribute(stamp_name)
          ]
        end

        private

        def create_methods(stamp)
          create_query_action(stamp) if stamp.actions[:query]
          create_set_action(stamp) if stamp.actions[:set]
          create_unset_action(stamp) if stamp.actions[:unset]
        end

        def create_query_action(stamp)
          class_eval <<-METHOD_DEF
            def #{stamp.actions[:query]}
              !!#{stamp.name}_at
            end
          METHOD_DEF
        end

        def create_set_action(stamp)
          class_eval <<-METHOD_DEF
            def #{stamp.actions[:set]}(by_user = nil)
              set_time_stamp(:#{stamp.name}, default_time)
              set_user_stamp(:#{stamp.name}, by_user || default_user)
              self.save!
              self
            end
          METHOD_DEF
        end

        def create_unset_action(stamp)
          class_eval <<-METHOD_DEF
            def #{stamp.actions[:unset]}
              set_time_stamp(:#{stamp.name}, nil)
              set_user_stamp(:#{stamp.name}, nil)
              self.save!
              self
            end
          METHOD_DEF
        end
      end
    end

    private

    def default_time
      Time.current
    end

    def default_user
      Thread.current[:current_user]
    end

    def set_time_stamp(stamp_name, at_time)
      time_attr = self.class.time_stamp_attribute(stamp_name)
      write_attribute(time_attr.to_s, at_time) if has_attribute?(time_attr)
    end

    def set_user_stamp(stamp_name, by_user)
      user_attr = self.class.user_id_stamp_attribute(stamp_name)
      write_attribute(user_attr.to_s, by_user&.id) if has_attribute?(user_attr)
    end
  end
end

ActiveRecord::Base
  .include(ActiveRecord::RecordStamps)
