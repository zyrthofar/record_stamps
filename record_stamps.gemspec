Gem::Specification.new do |s|
  s.name = 'record_stamps'
  s.version = '0.1.1'
  s.date = '2017-09-30'
  s.summary = 'Stamp tell who and when a user last did something.'
  s.description = 'Stamps tell who and when a user last did something.'
  s.authors = ['Zyrthofar']
  s.email = 'zyrthofar@gmail.com'
  s.files = [
    'lib/record_stamps.rb',
    'lib/active_record/connection_adapters/schema_statements_stamps.rb',
    'lib/active_record/connection_adapters/table_definition_stamps.rb',
    'lib/active_record/connection_adapters/table_stamps.rb',
    'lib/active_record/record_stamps.rb',
    'lib/record_stamps/record_stamp.rb'
  ]
  # s.homepage = 'http://rubygems.org/gems/hola'
  s.license = 'MIT'
end
